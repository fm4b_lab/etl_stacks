# Python GUI to do image stacks in a confocal spinning disk microscope modified with an electrically tunable lens

## Description
We have a confocal spinning disk (Yokogawa CSU-A1) with a CMOS camera (Photometrics  PRIME 95B, Gataca Systems, https://www.gataca-systems.com/). The spinning disk’s original configuration uses the Micromanager and a laser control program to acquire the confocal images. 

We incorporated an electrically tunable lens (ETL, EL-10-30-Ci-Vis-LD, Optotune) with an offset lens (OL) of negative focal distance (LC4232, Thorlabs) into the optical path. The ETL/OL assembly was mounted right after the objective to change the focal distance by changing the current applied to the ETL. 

Although it is possible to install a MicroManager plugin to control the ETL from the Micromanager, the operator should manually change the current and save individual images to record a stack at different focal planes.

The files included in this project allow recording stacks automatically by changing the focal distance of the ETL. It is designed to work simultaneously with the Micromanager and the laser control software.  The code is specific to our system but can be easily modified for any other system using an ETL with minimum modifications. 

## The GUI

![GUI](./aux/timeLapseRunning.PNG)

 When the program is launched, the ETL start and final current correspond to the ETL's full range of operation. The GUI has a slider to change the ETL focus to get a sharp image. The exposure time can be modified manually. The image contrast is automatically corrected. 

To make a stack, the user should input the initial and final currents of the ETL and the number of images per stack. Additionally, one can do time lapses of stacks.

There is a large edit box to write additional information about the experiment. When saving, a folder named `files_[date]_[time]/` is created inside `./pics`. Then, a log file containing relevant information plus the additional info entered by the user is created before recording the images.

The image shows an example where three stacks of five images were recorded.


## Installation and usage

You may download or clone the project folder to your host computer. Then, create a `conda` environment with the information provided in the `environment.yml` file. After that, you should be able to run the `main.py` file. 

Nonetheless, bear in mind that this program is written for a camera Prime 95B (Photometrics) using the PYVCAM package. Therefore, some modification is likely required for different systems. 

The GUI template was developed in Qt Designer 6.2.4 (`gui_etl.ui`), then exported to a Python file using the `pyuic5` command. It is generally easier to modify the `.ui` than the `.py` file.  


## Authors and acknowledgment
This program was originally written by Jorge Rodriguez Ramos (jorge.r.ramos@outlook.com) and Felix Rico (felix.rico@inserm.fr). This project received funding from the European Research Council (ERC, Grant Agreement No. 772257).

