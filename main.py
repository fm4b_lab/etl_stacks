# -*- coding: utf-8 -*-
"""
Created on Tue Jun  7 12:36:24 2022

@author: Jorge Rodriguez Ramos, jorge.r.ramos@outlook.com 

"""

import sys
import numpy as np
import os
import matplotlib.pyplot as plt

from datetime import datetime
import time 

from opto import Opto  # For the Optotune

import cv2 as cv  # For handling the Camera

from pyvcam import pvc
from pyvcam.camera import Camera

import qimage2ndarray

from PyQt5.QtGui import (
    QPixmap, QImage,
    )
from PyQt5.QtWidgets import (
    QMainWindow, QApplication,
    )
from PyQt5.QtCore import (
    QThread, pyqtSignal, Qt,
    )

from gui_etl import Ui_MainWindow # modified

# ===== CAMERA =====
# Initialize PVCAM
pvc.init_pvcam()
camera = next(Camera.detect_camera())
camera.open()
print(camera)  # Show camera name 
camera.binning = 1

class Window(QMainWindow, Ui_MainWindow):
    
    # ===== ETL ======
    etlPresent = True    
    tLapse = False

    def __init__(self, parent=None):
        super().__init__(parent)
        self.setupUi(self)
        
        if self.etlPresent: 
            self.startETL()
            self.slider.valueChanged.connect(self.updateETL)
                 
        # change exposure time    
        self.exposure.textEdited.connect(self.adjust_exposure_slot)

        # live camera as separate thread
        self.workerLive = WorkerLive() # create live worker
        
        #start exposure time
        self.workerLive.exposure_worker = int(self.exposure.text())
        
        self.workerLive.start() # start live
        self.workerLive.imageUpdate.connect(self.imageUpdateSlot)   
        
        # save single image
        self.saveSingle.clicked.connect(self.save_single_slot)
        
        # update time lapse status
        self.checkBox.stateChanged.connect(self.tlapse_update_slot)
        
        # save images stack
        self.saveStack.clicked.connect(self.save_stack_slot)
        
    # ==== SLOTS =====         
    def create_save_dir(self, stack = False):
        """
        Creates folder to store file, and includes the content in the additioal 
        information box.
        
        stack : boolean
                true when saving a stack
        
        Returns
        -------
        path : string
            path to folder with format files_date_and_time where images and log-file 
            will be stored.
        now : string
            date and time to use in filenames.

        """
        now = str(datetime.now()).replace(' ','_')[:-7].replace(':','.')
        path = './pics/' + 'files_' + now 
        # print(path)
        os.mkdir(path)
        
        log_name = path + '/log_' + now + '.txt'
        with open(log_name,'w') as f: # use 'w', since it will be opened once
            # add info
            if stack == False:
                etl_str =  str(np.round(self.etl.current()))
                general_info = 'ETL current (mV): ' + etl_str +'\n'
                
            else:
                i_start = self.i_start.text()
                i_end = self.i_end.text()
                n_stack = self.n_stack.text()
                n_lapse = self.n_lapse.text()
                if self.tLapse:
                    tLapseInfo = '\n \t number os stacks: ' + n_lapse
                else:
                    tLapseInfo = ' '
                general_info = ' '.join(['ETL stack info:\n \t start (mV): ', i_start,
                                        '\n \t end (mV): ',i_end, 
                                        '\n \t number of images in stack:', n_stack,
                                        tLapseInfo,'\n'])

                        
            f.write(general_info)
         
            # include additional info
            additional_info = self.logEdit.toPlainText()

            f.write(additional_info)
            
            # print in console es reference
            print('LOG INFO: \n=====\n')
            print(general_info)
            print(additional_info)

        return path, now
    
    def tlapse_update_slot(self):
        if self.checkBox.isChecked():
            self.tLapse = True
            print('Time lapse enabled')
        else:
            self.tLapse = False
            print('Time lapse disabled')
        
    def save_single_slot(self):
        path, now = self.create_save_dir()
        print('Data saved to:\n', path)
        
        # stop live image
        self.workerLive.stop()
               
        if camera.is_open:             
            img = camera.get_frame(exp_time=int(self.exposure.text()))
            filename = path + '/img_' + now + '.tif'
            cv.imwrite(filename, img)

            # restart live image 
            self.workerLive.start()
            self.workerLive.imageUpdate.connect(self.imageUpdateSlot)   
        
            
    def save_stack_slot(self):
        # when saving stacks, filenames has the recorded timestamp for each image
        print('started stack recording')
        path, now = self.create_save_dir(stack=True)
        #stop live image
        self.workerLive.stop()
        
        if self.etlPresent:
            etl_start_pos = self.etl.current() # pos before stack
            i_start = float(self.i_start.text())
            i_end = float(self.i_end.text())
        else:
            etl_start_pos, i_start, i_end = 0,0,100 # arbitrary
            
        # number of images in stack
        n_stack = int(self.n_stack.text())
        #number of time lapses    
        nLapse = int(self.n_lapse.text())
        
        if self.tLapse & (nLapse > 1):
            for idx in np.arange(1,nLapse+1):
                print('stack ', idx)
                # save one stack
                img_number = 0
                for i in np.linspace(i_start, i_end, n_stack):
                    if self.etlPresent:
                        self.etl.current(i)
                    
                    # wait time between images
                    waiting_time = float(self.t_stack.text())
                    time.sleep(1e-3*waiting_time)
                     
                    # get and save stack image
                    if camera.is_open:         
                        #save images
                        img = camera.get_frame(exp_time=self.workerLive.exposure_worker)
                        img_number += 1
                        now = str(datetime.now()).replace(' ','_')[:-7].replace(':','.')
                        filename = path + '/img_' + str(img_number)+ '_' + now +'_stack' + str(idx) +'.tif'
                        cv.imwrite(filename, img)
                        
                time.sleep(float(self.t_lapse.text()))
        else:
            print('single stack')
            # save one stack
            img_number = 0
            for i in np.linspace(i_start, i_end, n_stack):
                if self.etlPresent:
                    self.etl.current(i)
                
                # wait time between images
                waiting_time = float(self.t_stack.text())
                time.sleep(1e-3*waiting_time)
                
                # get and save stack image
                if camera.is_open:         
                    #save images
                    img = camera.get_frame(exp_time=self.workerLive.exposure_worker)
                    img_number += 1
                    now = str(datetime.now()).replace(' ','_')[:-7].replace(':','.')
                    filename = path + '/img_' + str(img_number)+ '_' + now + '.tif'
                    cv.imwrite(filename, img)
        
        #restart live image
        self.workerLive.start()
        print('stack saved in:\n', path)
                
        if self.etlPresent:
            self.etl.current(etl_start_pos)
        
    
    def adjust_exposure_slot(self, text):
        self.workerLive.exposure_worker = int(text) 
        
    def imageUpdateSlot(self, image):
        self.pic.setPixmap(QPixmap.fromImage(image))
    
    def closeEvent(self, *args, **kwargs):
        super().closeEvent(*args, **kwargs)
        # things to do when closing the window
        if self.etlPresent:
            self.etl.close(soft_close=True) # close ETL
            print("ETL closed!")
        
        self.workerLive.stop()     
        # close the camera
        camera.close()
        pvc.uninit_pvcam()
        print('Camera closed!')

    def updateETL(self, value): 
        self.etl.current(value)

    def startETL(self):
        # start ETL
        self.etl = Opto(port='COM3') # 'COM3' in spinning disk computer 
        self.etl.connect()
        print('ETL is ON')
        maxI = self.etl.current_upper()  # mV
        minI = self.etl.current_lower()  # mV
        ival = 150.0  # starting current ETL
        self.etl.current(ival) 
        
        # pass ETL limits and value to slider 
        self.slider.setMinimum(int(minI))
        self.slider.setMaximum(int(maxI))
        self.slider.setValue(int(ival))
        
        #pass ETL limits to stack edit text as reference
        self.i_start.setText(str(np.round(minI)))
        self.i_end.setText(str(np.round(maxI)))

       
# ===== ADDITIONAL THREAD =====
        
class WorkerLive(QThread):
    imageUpdate = pyqtSignal(QImage)   
    equalize = pyqtSignal(int)
    exposure_worker = pyqtSignal(int)
    
    def run(self):             
        self.ThreadActive = True
        
        flag = False # auxiliar for debugging
        
        while self.ThreadActive:           
            if camera.is_open:             
                # get frame 
                try:
                    image = camera.get_frame(exp_time=self.exposure_worker)    
    
                    # convert central region to QImage
                    pic = qimage2ndarray.array2qimage(image[150:1050,300:900], normalize=True)
                    
                    #scale image to show in GUI
                    px = 600 # pixels
                    pic = pic.scaled(px, px, Qt.KeepAspectRatio)
                    
                    # emit QImage
                    self.imageUpdate.emit(pic)
                    
                    #tmp test    
                    if flag:
                        print('Camera is open')
                        plt.imshow(image,cmap='gray') 
                        plt.title('Ref img to test camera')
                        plt.axis('off')
                        plt.show()
                                            
                        print(image.dtype)
                        flag = False
                        
                except RuntimeError: # there is a runtime error inside pvc, which can be ignored
                    pass

                
    def stop(self):
        self.ThreadActive = False


#=================== MAIN ===================================            
if __name__ == "__main__":
    app = QApplication(sys.argv)
    win = Window()
    win.show()
    sys.exit(app.exec())
    

